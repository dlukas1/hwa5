import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;


   public Tnode(){}
   public Tnode (String n, Tnode l, Tnode r) {
      name = n;
      firstChild = l;
      nextSibling = r;
   }
   public Tnode (String n) { name = n; }

   private static String SWAP = "SWAP";
   private static String ROT = "ROT";

   private static Set<String> ops = new HashSet<>(Arrays.asList("+", "-", "*", "/", SWAP, ROT));

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(name);
      Tnode node = firstChild;
      if (firstChild != null)
         sb.append('(');
      while (node != null) {
         sb.append(node.toString());
         node = node.nextSibling;
         if (node != null)
            sb.append(',');
      }
      if (firstChild != null)
         sb.append(')');
      return sb.toString();
   }

   public static Tnode buildSubTree(Tnode root, Tnode firstChild, Tnode nextSibling){
      Tnode subRoot = root;
      subRoot.firstChild = firstChild;
      subRoot.firstChild.nextSibling = nextSibling;
      return subRoot;
   }

   public static Tnode buildFromRPN(String pol) {
      if (!validateString(pol)) {
         throw new RuntimeException("Illegal input " + pol + ", should contain only 0-9 and +-/*");
      }

      List<Tnode> allNodes = getAllNodes(pol);

      if (allNodes.size()>1 && !ops.contains(allNodes.get(allNodes.size()-1).name)) {
         throw new RuntimeException("Illegal input " + pol + ", last element should be of [+-/*]");
      }

      Stack<Tnode> temp = new Stack<>();

      for (Tnode node : allNodes) {
         if(ops.contains(node.name)){
            if(temp.size()< 2){
               throw new RuntimeException("Illegal input " + pol + ", too few operands");
            }
            if (SWAP.equals(node.name)) {
               Tnode top = temp.pop();
               Tnode next = temp.pop();
               temp.push(top);
               temp.push(next);
            }
            else if(ROT.equals(node.name)){
               if (temp.size() < 3) {
                  throw new RuntimeException("Illegal input " + pol + ", ROT expects 3 elements");
               }
               Tnode top = temp.pop();
               Tnode middle = temp.pop();
               Tnode last = temp.pop();
               temp.push(middle);
               temp.push(top);
               temp.push(last);

            }
            else {
               Tnode sibling = temp.pop();
               Tnode child = temp.pop();
               temp.push(buildSubTree(node, child, sibling));
            }
         } else {
            temp.add(node);
         }
      }

      return temp.pop();
   }

   public static void main (String[] param) {
      String simple = "2 5 9 ROT + SWAP -" ;
      Tnode node = buildFromRPN(simple);
      System.out.println(node.toString());//"2 5 9 ROT + SWAP -"  => -(+(9,2),5)
   }

   public static List<Tnode> getAllNodes(String s){
      String [] ns = s.split(" ");
      List<Tnode> nodes = new ArrayList<>();
      for (int i = 0; i < ns.length ; i++) {
         //fill array with nodes(name)
         nodes.add(new Tnode(ns[i]));
      }
      return nodes;
   }


   public static boolean validateString(String s){
      String regex = "[\\d\\s\\(\\)\\+\\-\bSWAP\b\bROT\b\\*\\/\\.]+";
      return s != null && !s.trim().isEmpty() && s.matches(regex);
   }
}

